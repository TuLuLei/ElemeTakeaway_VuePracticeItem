export default {
  user: {
    id: 1,
    avatar: 'https://avatar.gitee.com/uploads/63/1423163_cc_vinci.png!avatar60?1523760854',
    username: 'Frank',
    phone: 18839121422,
    coupon: ['满100减1元', '满50减0.5元', '全场5元券', '下午茶2元券', '宵夜3元券'],
    integral: 1000, // 积分
    wallet: 10889// 钱包余额
  },
  seller: JSON.parse(localStorage.getItem('seller')) || {},
  shopcart: {}
}
