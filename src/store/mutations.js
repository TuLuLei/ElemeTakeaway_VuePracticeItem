import * as types from './mutation-type'
export default {
  [types.ADD_COUPON] (state, coupon) {
    state.user.coupon.push(coupon)
  },
  [types.UPDATE_INTEGRAL] (state, integral) {
    state.integral += integral
  },
  [types.ADD_SELLER] (state, seller) {
    localStorage.setItem('seller', JSON.stringify(seller))
    state.seller = seller
  },
  [types.INIT_SHOPCART] (state, list) {
    let shopcart = sessionStorage.getItem('shopcart')
    if (shopcart) {
      state.shopcart = JSON.parse(shopcart)
    } else {
      for (let i of list) {
        state.shopcart[i.id] = []
      }
      sessionStorage.setItem('shopcart', JSON.stringify(state.shopcart))
    }
  },
  [types.ADD_SHOPCART] (state, {sellerId, goods}) {
    let products = state.shopcart[sellerId]
    if (products) {
      // 如果这个商家之前已经在我们的购物车里了
      for (let i in products) {
        if (products[i].title === goods.title) {
          // 如果我们这次点的菜，之前已经点过了，那么就在数量上 +1
          products[i].count++
          break
        }
      }
      // 我们这次点的才，之前没点过，添加上去，count为1
      products.push(goods)
    } else {
      // 这个商家之前没在购物车里:就用这个菜作为该商家数组里的第一件
      state.shopcart[sellerId] = [goods]
    }
    sessionStorage.setItem('shopcart', JSON.stringify(state.shopcart))
  },
  [types.DEL_SHOPCART] (state, {sellerId, goods}) {
    // 取出这个商家的所有商品
    let products = state.shopcart[sellerId]
    for (let i in products) {
      if (products[i].title === goods.title) {
        products[i].count--
        if (products[i].count === 0) {
          products.splice(parseInt(i), 1)
        }
        break
      }
    }
    sessionStorage.setItem('shopcart', JSON.stringify(state.shopcart))
  }
}
