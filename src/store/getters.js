// 返回当前登录用户信息
export const user = state => state.user
// 返回当前商家信息
export const seller = state => state.seller
// 返回所有的购物车信息
export const shopcart = state => state.shopcart
