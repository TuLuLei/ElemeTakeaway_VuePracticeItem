import * as types from './mutation-type'
export const activity = ({commit}, payload) => {
  setTimeout(() => {
    commit(types.ADD_COUPON, payload.coupon)
    commit(types.UPDATE_INTEGRAL, 10)
  }, 2000)
}
