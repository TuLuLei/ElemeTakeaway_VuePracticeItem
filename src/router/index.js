import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/home/Home'
import Take from '@/components/home/take/Take'
import Order from '@/components/home/order/Order'
import Mine from '@/components/home/mine/Mine'
import Find from '@/components/home/find/Find'
import Seller from '@/components/seller/Seller'
Vue.use(Router)

const router = new Router({
  linkActiveClass: 'active',
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/home/take'
    }, {
      path: '/home',
      name: 'Home',
      component: Home,
      children: [
        {
          path: 'take',
          name: 'Take',
          component: Take
        }, {
          path: 'find',
          name: 'Find',
          component: Find,
          meta: {
            title: '发现'
          }
        }, {
          path: 'order',
          name: 'Order',
          component: Order,
          meta: {
            title: '订单'
          }
        }, {
          path: 'mine',
          name: 'Mine',
          component: Mine,
          meta: {
            title: '我的'
          }
        }
      ]
    }, {
      path: '/seller',
      name: 'Seller',
      component: Seller
    }, {
      path: '/helloworld',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})

router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || '饿了么'
  next()
})
export default router
